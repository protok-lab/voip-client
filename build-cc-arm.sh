#!/bin/bash
source /opt/st/stm32mp1-sss/3.1-snapshot/environment-setup-cortexa7t2hf-neon-vfpv4-ostl-linux-gnueabi

echo "build lib-re"
cd re
make OS=linux
cd ..

echo "build lib-rem"
cd rem
make OS=linux
cd ..

echo "build baresip"
cd baresip
make OS=linux LIBREM_PATH=../rem/ LIBRE_PATH=../re/ USE_OPUS=1

